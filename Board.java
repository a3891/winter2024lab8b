import java.util.Random;
public class Board {
	
	private Tile[][] grid;
	final int size = 5;
	
	public Board() {
		grid = new Tile[size][size];
		Random rng = new Random();
		for (int a = 0; a < grid.length; a++) {
			for (int b = 0; b < grid[a].length; b++) {
				grid[a][b] = Tile.BLANK;
			}
			int random_index = rng.nextInt(grid[a].length);
			grid[a][random_index] = Tile.HIDDEN_WALL;
		}
	}
	
	public String toString() {
		StringBuilder represantation = new StringBuilder();
		for (Tile[] row : grid) {
			for (Tile tile : row) {
				represantation.append(tile.getName());
				represantation.append(" ");
			}
			represantation.append("\n");
		}
		return represantation.toString();
	}
	
	public int placeToken(int row, int col) {
		int score = 0;
		if (!(row >= 1 && row <= size && col >= 1 && col <= size)) {
			score = -2;
		}
		else if (grid[row-1][col-1] == Tile.CASTLE || grid[row-1][col-1] == Tile.WALL) {
			score = -1;
		}
		else if (grid[row-1][col-1] == Tile.HIDDEN_WALL) {
			grid[row-1][col-1] = Tile.WALL;
			score = 1;
		}
		else {
			grid[row-1][col-1] = Tile.CASTLE;
		}
		return score;
	}

}