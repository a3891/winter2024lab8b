import java.util.Scanner;
public class BoardGameApp {
	public static void main (String args[]) {
		
		//Tile tile1 = Tile.BLANK;
		//Tile tile2 = Tile.WALL;
		//System.out.println(tile1);
		//System.out.println(tile2);
		//Board board = new Board();
		//System.out.println(board);
		//Scanner scan = new Scanner(System.in);
        //System.out.println("Enter row and column");
        //int row = scan.nextInt();
        //int col = scan.nextInt();
        //int score = board.placeToken(row, col);
        //System.out.println("Score: " + score);
        //System.out.println(board);
		
		//main game
		System.out.println("Welcome !");
		int numCastles = 5;
		int turns = 0;
		Board board = new Board();
		
		while (numCastles > 0 && turns < 8) {
			System.out.println(board);
			System.out.println("Turn: " + turns);
			System.out.println("Number of castles: " + numCastles);
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter row and column.");
			int row = scan.nextInt();
			int col = scan.nextInt();
			int score = board.placeToken(row, col);
			if (score < 0) {
                System.out.println("Invalid input.");
            } 
			else if (score == 1) {
                System.out.println("There was a wall at that tile. Turns are reduced by 1.");
                turns++;
            } 
			else {
                System.out.println("Castle tile was successfully placed.");
                numCastles--;
                turns++;
            }
		}
		
		System.out.println("Game over!");
		System.out.println(board);
		if (numCastles == 0) {
			System.out.println("You won!");
		}
		else {
			System.out.println("You lost!");
		}
	}
}